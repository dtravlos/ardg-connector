﻿using FSUIPC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARDG_Connector
{
    public class OffsetRegistrant
    {


        private static OffsetRegistrant instance;
        public static OffsetRegistrant Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new OffsetRegistrant();
                }
                return instance;
            }
        }
        
        public Dictionary<string, PeleOffSetInt> RegisterredIntOffsets = new Dictionary<string, PeleOffSetInt>();
        public Dictionary<string, PeleOffSetByte> RegisterredByteOffsets = new Dictionary<string, PeleOffSetByte>();
        public Dictionary<string, PeleOffSetString> RegisterredStringOffsets = new Dictionary<string, PeleOffSetString>();

    }


    public class PeleOffSetInt : Offset<int>
    {
  

        public PeleOffSetInt(int Adress,string Name) : base(Adress)
        {
            register(Name);
        }

        public PeleOffSetInt(int adr, int lenght,string Name) : base(adr, lenght)
        {
            register(Name);
        }

        public int lastValue = -12345678;
        public int PeleValue { get
            {
                lastValue = Value;
                return Value;
            }
                
        }

        //private bool changed = false;

        public bool Changed
        {
            get { return lastValue != Value; }
         
        }


        public void register(string Name)
        {
            OffsetRegistrant.Instance.RegisterredIntOffsets.Add(Name, this);
        }
     
    }

    public class PeleOffSetByte : Offset<byte[]>
    {


        public PeleOffSetByte(int Adress, string Name) : base(Adress)
        {
            register(Name);
        }

        public PeleOffSetByte(int adr, int lenght, string Name) : base(adr, lenght)
        {
            register(Name);
        }


        public void register(string Name)
        {
            OffsetRegistrant.Instance.RegisterredByteOffsets.Add(Name, this);
        }

    }

    public class PeleOffSetString : Offset<string>
    {


        public PeleOffSetString(int Adress, string Name) : base(Adress)
        {
            register(Name);
        }

        public PeleOffSetString(int adr, int lenght, string Name) : base(adr, lenght)
        {
            register(Name);
        }

        public string lastValue = "";
        public string PeleValue
        {
            get
            {
                lastValue = Value;
                return Value;
            }

        }

        //private bool changed = false;

        public bool Changed
        {
            get { return lastValue != Value; }

        }

        public void register(string Name)
        {
            OffsetRegistrant.Instance.RegisterredStringOffsets.Add(Name, this);
        }

    }


}
