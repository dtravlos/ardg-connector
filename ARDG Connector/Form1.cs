﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using FSUIPC;
using System.Reflection;

namespace ARDG_Connector
{
    public partial class Form1 : Form
    {
        private static readonly string AppTitle = "ARDG Connector";

        //Custom control send
        private Offset<int> controlParam = new Offset<int>("sendControl", 0x3114, true);
        private Offset<int> sendControl = new Offset<int>("sendControl", 0x3110, true); // Must be declared AFTER 3114. 

        //                           "EVT_LTS_TAXISW",      "536870912"
        private void SendControlToFS(int controlNumber, int parameterValue)
        {
            
            this.sendControl.Value = controlNumber;
            this.controlParam.Value = parameterValue;
            FSUIPCConnection.Process("sendControl");
        }

        Dictionary<string, int> pmdg_events = PMDG.GetEvents();

        //
        // Offsets to read
        //
        // General
        private PeleOffSetByte fsLocalDateTime = new PeleOffSetByte(0x0238, 10, "fsLocalDateTime");

        //MCP
        //Led indicators
        private PeleOffSetInt MCP_annunATArm = new PeleOffSetInt(0x653A, "MCP_annunATArm");
        private PeleOffSetInt MCP_annunFD = new PeleOffSetInt(0x6538, "MCP_annunFD");
        private PeleOffSetInt MCP_annunN1 = new PeleOffSetInt(0x653B, "MCP_annunN1");
        private PeleOffSetInt MCP_annunSPEED = new PeleOffSetInt(0x653C, "MCP_annunSPEED");
        private PeleOffSetInt MCP_annunVNAV = new PeleOffSetInt(0x653D, "MCP_annunVNAV");
        private PeleOffSetInt MCP_annunLVL_CHG = new PeleOffSetInt(0x653E, "MCP_annunLVL_CHG");
        private PeleOffSetInt MCP_annunHDG_SEL = new PeleOffSetInt(0x653F, "MCP_annunHDG_SEL");
        private PeleOffSetInt MCP_annunLNAV = new PeleOffSetInt(0x6540, "MCP_annunLNAV");
        private PeleOffSetInt MCP_annunVOR_LOC = new PeleOffSetInt(0x6541, "MCP_annunVOR_LOC");
        private PeleOffSetInt MCP_annunAPP = new PeleOffSetInt(0x6542, "MCP_annunAPP");
        private PeleOffSetInt MCP_annunALT_HOLD = new PeleOffSetInt(0x6543, "MCP_annunALT_HOLD");
        private PeleOffSetInt MCP_annunVS = new PeleOffSetInt(0x6544, "MCP_annunVS");
        private PeleOffSetInt MCP_annunCMD_A = new PeleOffSetInt(0x6545, "MCP_annunCMD_A");
        private PeleOffSetInt MCP_annunCMD_B = new PeleOffSetInt(0x6547, "MCP_annunCMD_B");
        //Screens
        private PeleOffSetString MCP_Course = new PeleOffSetString(0x6520, "MCP_Course");
        private PeleOffSetString MCP_IASMach = new PeleOffSetString(0x6524, "MCP_IASMach");
        private PeleOffSetString MCP_Heading = new PeleOffSetString(0x652C, "MCP_Heading");
        private PeleOffSetString MCP_Altitude = new PeleOffSetString(0x652E, "MCP_Altitude");

        // APU Needle
        private Offset<float> APU_EGTNeedle = new Offset<float>(0x648C);


        public Form1()
        {
            InitializeComponent();
        }

        // Open FSUIPC Connections
        private void connectFSUIPC_Click(object sender, EventArgs e)
        {
            openFSUIPC();
        }

        // Opens FSUIPC - if all goes well then starts the 
        // timer to drive start the main application cycle.
        // If can't open display the error message.
        private void openFSUIPC()
        {
            try
            {
                // Attempt to open a connection to FSUIPC (running on any version of Flight Sim)
                FSUIPCConnection.Open();
                // Opened OK so disable the Connect button
                this.connectFSUIPC.Enabled = false;
                // Start the timer ticking to drive the rest of the application
                this.mainTimer.Interval = 1000;
                this.mainTimer.Enabled = true;
            }
            catch (Exception ex)
            {
                // Badness occurred - show the error message
                MessageBox.Show(ex.Message, AppTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                FSUIPCConnection.Close();
                Debug.WriteLine(ex.Message);
            }
        }

        // Application is unloading so call close to cleanup the 
        // UNMANAGED memory used by FSUIPC. 
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            FSUIPCConnection.Close();
        }

        private void mainTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                FSUIPCConnection.Process();
                //Write datetime
                short year = BitConverter.ToInt16(fsLocalDateTime.Value, 8);
                DateTime fsTime = new DateTime(year, 1, 1, fsLocalDateTime.Value[0], fsLocalDateTime.Value[1], fsLocalDateTime.Value[2]);
                short dayNo = BitConverter.ToInt16(fsLocalDateTime.Value, 6);
                fsTime = fsTime.Add(new TimeSpan(dayNo - 1, 0, 0, 0));
                this.fsLocalTime.Text = fsTime.ToString("dddd, MMMM dd yyyy hh:mm:ss");

                //byte atLed = 
                // debug(MCP_annunATArm.Value.ToString());

                //debug(APU_EGTNeedle.Value.ToString());

                //read serial
                //"EVT_LTS_TAXI_SW:MOUSE_FLAG_LEFTSINGLE"
                SendControlToFS(pmdg_events["EVT_OH_LIGHTS_TAXI"], pmdg_events["MOUSE_FLAG_LEFTSINGLE"]);
                
                foreach (var item in OffsetRegistrant.Instance.RegisterredIntOffsets)
                {
                    if (item.Value.Changed)
                    {
                        debug(item.Key + ":" + item.Value.PeleValue.ToString());
                    }
                   
                }

                foreach (var item in OffsetRegistrant.Instance.RegisterredByteOffsets)
                {
                    string result = System.Text.Encoding.UTF8.GetString(item.Value.Value);
                    debug(item.Key +":" + result);
                }


            }
            catch (FSUIPCException ex)
            {
                if (ex.FSUIPCErrorCode == FSUIPCError.FSUIPC_ERR_SENDMSG)
                {
                    // Send message error - connection to FSUIPC lost.
                    // Show message, disable the main timer loop and relight the 
                    // connection button:
                    // Also Close the broken connection.
                    this.mainTimer.Enabled = false;
                    this.connectFSUIPC.Enabled = true;
                    FSUIPCConnection.Close();
                    MessageBox.Show("The connection to Flight Sim has been lost.", AppTitle, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    // not the disonnect error so some other baddness occured.
                    // just rethrow to halt the application
                    throw ex;
                }
            }
            catch (Exception ee)
            {
                // Sometime when the connection is lost, bad data gets returned 
                // and causes problems with some of the other lines.  
                // This catch block just makes sure the user doesn't see any
                // other Exceptions apart from FSUIPCExceptions.
            }
        }


      

        private void debug(string line)
        {
            this.debugTextBox.AppendText(line);
            this.debugTextBox.AppendText(Environment.NewLine);
        }
    }
}
