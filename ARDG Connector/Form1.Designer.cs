﻿namespace ARDG_Connector
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.connectFSUIPC = new System.Windows.Forms.Button();
            this.mainTimer = new System.Windows.Forms.Timer(this.components);
            this.debugTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fsLocalTime = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // connectFSUIPC
            // 
            this.connectFSUIPC.BackColor = System.Drawing.SystemColors.HotTrack;
            this.connectFSUIPC.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectFSUIPC.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.connectFSUIPC.Location = new System.Drawing.Point(165, 12);
            this.connectFSUIPC.Name = "connectFSUIPC";
            this.connectFSUIPC.Size = new System.Drawing.Size(259, 54);
            this.connectFSUIPC.TabIndex = 1;
            this.connectFSUIPC.Text = "Connect to FSUIPC";
            this.connectFSUIPC.UseVisualStyleBackColor = false;
            this.connectFSUIPC.Click += new System.EventHandler(this.connectFSUIPC_Click);
            // 
            // mainTimer
            // 
            this.mainTimer.Tick += new System.EventHandler(this.mainTimer_Tick);
            // 
            // debugTextBox
            // 
            this.debugTextBox.Location = new System.Drawing.Point(23, 119);
            this.debugTextBox.Multiline = true;
            this.debugTextBox.Name = "debugTextBox";
            this.debugTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.debugTextBox.Size = new System.Drawing.Size(537, 413);
            this.debugTextBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Local FS Time:";
            // 
            // fsLocalTime
            // 
            this.fsLocalTime.Location = new System.Drawing.Point(121, 88);
            this.fsLocalTime.Name = "fsLocalTime";
            this.fsLocalTime.Size = new System.Drawing.Size(355, 13);
            this.fsLocalTime.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 556);
            this.Controls.Add(this.fsLocalTime);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.debugTextBox);
            this.Controls.Add(this.connectFSUIPC);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "ARDG Connector";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button connectFSUIPC;
        private System.Windows.Forms.Timer mainTimer;
        private System.Windows.Forms.TextBox debugTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label fsLocalTime;
    }
}

